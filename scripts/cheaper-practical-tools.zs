
craftingTable.addShapedMirrored("cheaper_practical_tools_iron_hammer",
 <item:practicaltools:iron_hammer>,
 [[<item:minecraft:iron_block>,<item:minecraft:stick>,<item:minecraft:iron_block>],
 [<item:minecraft:air>,<item:minecraft:stick>,<item:minecraft:air>],
 [<item:minecraft:air>,<item:minecraft:stick>,<item:minecraft:air>]]);

craftingTable.addShapedMirrored("cheaper_practical_tools_diamond_hammer",
 <item:practicaltools:diamond_hammer>,
 [[<item:minecraft:diamond_block>,<item:minecraft:stick>,<item:minecraft:diamond_block>],
 [<item:minecraft:air>,<item:minecraft:stick>,<item:minecraft:air>],
 [<item:minecraft:air>,<item:minecraft:stick>,<item:minecraft:air>]]);

