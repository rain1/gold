import crafttweaker.api.BracketHandlers;

val quarkChestTypes = ["oak","spruce","birch","jungle","acacia","dark_oak","crimson","warped","nether_brick","purpur","prismarine","mushroom"];

for chestType in quarkChestTypes {
    val chest = BracketHandlers.getItem("quark:" + chestType + "_chest");
    craftingTable.addShaped(chestType + "_in_travelers_backpack_recipe", <item:travelersbackpack:standard>,
                 [[<item:minecraft:leather>, <item:minecraft:gold_ingot>, <item:minecraft:leather>],
                  [<item:travelersbackpack:backpack_tank>, chest, <item:travelersbackpack:backpack_tank>],
                  [<item:minecraft:leather>, <item:travelersbackpack:sleeping_bag>, <item:minecraft:leather>]]);
}
