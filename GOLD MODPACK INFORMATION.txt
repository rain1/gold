1.16.4 modpack:

# Quality of Life
JEI (items and recipes)
JER (ore gen graphs, extra info)
Appleskin (food info tooltips)
MouseTweaks (drag crafting)
Hwyla (what am i looking at - config this, make it smaller)
WAWLA (extra looking at)
Waila Harvestability
Corpse
Surrounding Indicators
Travellers Index (adds a button to escape menu)
GameMenuModOption (required to confid Xaeros Minimap)
BetterAdvancements (improve advancement ui)
AdvancementBook
XaerosMinimap (config this: put it in top right)
Xaeros World map
Waystones2Waypoints (Xeros + Waystone integration)
CraftTweaker (if we need to fix up any recipes from modpack conflict stuff)
TrashSlot (so you can bin useless items)
CorpseComplex (enables us to config the server to keep xp)

# Items/Tools
Waystones (teleporters)
Nature's Compass (locate specific biomes)
Camera (take photos)
Travellers backpack
Mining Helmet
Paraglider
Storage Overhaul (chests for each wood type and chests that hold MUCH more!)
Scuba Gear
Craftable Nametag
Advanced Finders (compasses to locate ores)
Inspirations (gives us bookshelves) https://github.com/KnightMiner/Inspirations/wiki
Hammers and more (from Tetra)
Practical Tools (hammer)

# Mobs
Jellyfishing (sounds edited because they were horrible)

# Enchantments
Leap
Wonderful Enchantments

# Structures
Towers of the Wild
Workshops of doom (extra illager stuff)
Greek Fantasy (various greek myth creatures and structures)
Undergarden Dimension (stone brick portal activate with catalyst)







# Expansions

Quark https://quark.vazkii.net/#features

Astral Sorcery
get the Astral Tome
(Astral Sorcery is a mod created by HellFirePvP. It is a magic-themed mod based on constellations and star power, which means that most of the mod's content is accessed at nighttime. It adds shrines made of Marble dotted throughout the world. Some of the larger shrines house a floating crystal beneath it, which will allow players to begin their journey.)
https://minecraftguides.net/AS/index

Ice and Fire
Dragon roosts.
https://ice-and-fire-mod.fandom.com/wiki/Ice_and_Fire_Mod_Wiki

Ars Nouveau
Magic done with glyphs
(To get started with this mod, craft the Worn Notebook using a book and 1 Mana Gem.)
... Or would https://www.curseforge.com/minecraft/mc-mods/mana-and-artifice be better?

Dungeons+
https://www.curseforge.com/minecraft/mc-mods/dungeons-plus

Dungeon Crawl
https://www.curseforge.com/minecraft/mc-mods/dungeon-crawl

Applied Energistics 2
Transform items into energy and back
https://ae-mod.info/

# Deps
Citadel (for ice and fire)
observerlib (for astral)
curious (for astral)
autoreglib (quark)
Structure Gel API (Dungeons+)
looot (workshops of doom)
Gecko Lib (Ars Nouveau)
ForgeEndertech (Advanced finders)
Mantle (for Inspirations)
Patchouli (various mods add books)
mgui (Tetra)